package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/api/products")
    public ResponseEntity createProduct(@Valid @RequestBody Product product){
        Product savedProduct = productRepository.save(product);
        return ResponseEntity.status(201)
                .header("location", "http://localhost/api/products/" + savedProduct.getId())
                .build();
    }

    @GetMapping("/api/products/{productId}")
    public ResponseEntity getProduct(@Valid @PathVariable Long productId){
        Optional<Product> productOptional = productRepository.findById(productId);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(productOptional.get());
    }

    @ExceptionHandler
    public ResponseEntity handleException(RuntimeException e){
        return ResponseEntity.status(404).build();
    }
}